import axios from "axios";
import { BASE_URL, configHeaders } from "./config";

export const userService = {
  postLogin: (formLogin) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
      method: "POST",
      data: formLogin,
      headers: configHeaders(),
    });
  },
  postLRegister: (formRegister) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangKy`,
      method: "POST",
      data: formRegister,
      headers: configHeaders(),
    });
  },
};
