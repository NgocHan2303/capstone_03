import {
  GET_THEATER,
  GET_BANNER,
  GET_LICH_CHIEU,
} from "./../constant/movieConstant";

const initialState = {
  movies: [],
  banners: [],
  scheduleFilm: [],
};

export const movieReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_THEATER:
      return {
        ...state,
        movies: action.payload,
      };
    case GET_BANNER:
      return {
        ...state,
        banners: action.payload,
      };
    case GET_LICH_CHIEU:
      return {
        ...state,
        scheduleFilm: action.payload,
      };

    default:
      return state;
  }
};
