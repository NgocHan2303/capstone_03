import React from "react";
import { useSelector } from "react-redux";
import { localUserService } from "./../../services/localServices";
import "./Header.css";
import { NavLink } from "react-router-dom";

export default function Header() {
  let userInfo = useSelector((state) => {
    return state.userReducer.userInfo;
  });

  let handleLogoutClick = () => {
    localUserService.remove();
    window.location.href = "/";
  };

  let renderUserNav = () => {
    let btnCss =
      "px-5 py-2 rounded font-medium border-solid border-1 bg-gray-800 hover:bg-gray-950";
    if (userInfo) {
      // đã đăng nhập
      return (
        <>
          <span>{userInfo.hoTen}</span>
          <button className={btnCss} onClick={handleLogoutClick}>
            Đăng xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <NavLink to="/login">
            <button className={btnCss}>Đăng nhập</button>
          </NavLink>
          <NavLink to="/register">
            <button className={btnCss}>Đăng ký</button>
          </NavLink>
        </>
      );
    }
  };
  return (
    <div className="bg-blue-950 bg-opacity-80 text-white flex justify-between items-center fixed top-0 left-0 w-full h-20 shadow-md z-50  gap-5 font-thin">
      <NavLink to={"/"}>
        <div className="header_logo flex items-center m-5">
          <img
            src="https://img.icons8.com/plasticine/100/null/naruto.png"
            style={{ height: 80 }}
            alt="logo_img"
            className="logo_left"
          />
          <img
            src="https://img.icons8.com/plasticine/100/null/naruto.png"
            style={{ height: 80 }}
            alt="logo_img"
            className="logo_right"
          />
        </div>
      </NavLink>
      <div className="flex space-x-5 pr-5 font-medium">
        <NavLink className="hover:text-black" to="/lich-chieu">
          Lịch chiếu
        </NavLink>
        <NavLink className="hover:text-black" to="/cum-rap">
          Cụm rạp
        </NavLink>
        <NavLink className="hover:text-black" to="/tin-tuc">
          Tin tức
        </NavLink>
        <NavLink className="hover:text-black" to="/ung-dung">
          Ứng dụng
        </NavLink>
      </div>
      <div className="flex items-center space-x-5 pr-5 ">
        <div className="space-x-5 ">{renderUserNav()}</div>
      </div>
    </div>
  );
}
// Tab antd
// Viết hàm đăng xuất: remove localStorage, reload trang bằng window.location
