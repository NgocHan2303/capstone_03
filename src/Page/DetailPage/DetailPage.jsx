import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { movieService } from "./../../services/movieService";
import { Progress, Tabs } from "antd";
import { Container } from "@mui/material";

import { useDispatch, useSelector } from "react-redux";
import { getLichChieuAction, getTheater } from "../../redux/action/movieAction";
import ChildrenTabMovie from "../HomePage/TabMovie/ChildrenTabMovie";

export default function DetailPage() {
  const dispatch = useDispatch();
  let params = useParams();
  const [movie, setMovie] = useState({});
  const movies = useSelector((state) => state.movieReducer.movies);
  useEffect(() => {
    movieService
      .getDetailMovie(params.id)
      .then((res) => {
        console.log(res);
        setMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
    dispatch(getLichChieuAction(params.id));
    dispatch(getTheater());
  }, []);
  let renderHeThongRap = () => {
    return movies.map((heThongRap, index) => {
      return {
        key: index,
        label: <img src={heThongRap.logo} className="w-16" alt="" />,

        children: (
          <Tabs
            style={{ height: 600 }}
            tabPosition="left"
            items={heThongRap.lstCumRap.map((cumRap) => {
              return {
                key: cumRap.diaChi,
                label: (
                  <p
                    className="flex items-center justify-center"
                    style={{ height: "64px" }}
                  >
                    {cumRap.tenCumRap}
                  </p>
                ),
                children: <ChildrenTabMovie listMovie={cumRap.danhSachPhim} />,
              };
            })}
          />
        ),
      };
    });
  };
  return (
    <div>
      <Container>
        <div className="bg-cover bg-no-repeat pt-20">
          <div className=" flex items-center space-x-10">
            <img src={movie.hinhAnh} className="w-60 pl-10" alt="" />
            <div className="space-y-10">
              <p className="text-left font-bold">{movie.tenPhim}</p>
            </div>
            <div>
              <Progress
                type="circle"
                percent={movie.danhGia * 10}
                status="exception"
                strokeColor={{
                  "0%": "#108ee9",
                  "100%": "#87d068",
                }}
                format={(percent) => {
                  return percent / 10;
                }}
                size={100}
                className="font-bold"
              />
            </div>
          </div>
          <div>
            <Tabs
              style={{ height: 650 }}
              className="pt-5 border-black rounded-lg border-solid border-2 m-5"
              tabPosition="left"
              defaultActiveKey="1"
              items={renderHeThongRap()}
            />
          </div>
        </div>
      </Container>
    </div>
  );
}
