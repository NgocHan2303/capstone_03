import React from "react";
import { Container } from "@mui/material";
import "./Footer.css";

export default function Footer() {
  return (
    <div className="bg-blue-950 text-white">
      <Container className="MuiContainer-root">
        <footer class="bg-blue-950 rounded-lg shadow m-4">
          <div class="w-full max-w-screen-xl mx-auto p-4 md:py-8">
            <section id="lab_social_icon_footer">
              <link
                href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"
                rel="stylesheet"
              />
              <div className="container">
                <div className="text-center center-block">
                  <a href="https://www.facebook.com/bootsnipp">
                    <i
                      id="social-fb"
                      className="fa fa-facebook-square fa-3x social"
                    />
                  </a>
                  <a href="https://twitter.com/bootsnipp">
                    <i
                      id="social-tw"
                      className="fa fa-twitter-square fa-3x social"
                    />
                  </a>
                  <a href="https://plus.google.com/+Bootsnipp-page">
                    <i
                      id="social-gp"
                      className="fa fa-google-plus-square fa-3x social"
                    />
                  </a>
                  <a href="mailto:#">
                    <i
                      id="social-em"
                      className="fa fa-envelope-square fa-3x social"
                    />
                  </a>
                </div>
              </div>
            </section>

            <hr class="my-6 border-gray-200 sm:mx-auto dark:border-gray-700 lg:my-8" />
            <span class="block text-sm text-white sm:text-center dark:text-white">
              © 2023 . All Rights Reserved.
            </span>
          </div>
        </footer>
      </Container>
    </div>
  );
}
