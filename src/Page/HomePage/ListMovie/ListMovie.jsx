import React, { useEffect, useState } from "react";
import { movieService } from "./../../../services/movieService";
import CardMovie from "./CardMovie";
import { Container } from "@mui/material";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export default function ListMovie() {
  let [movies, setMovies] = useState([]);
  useEffect(() => {
    movieService
      .getMovieList()
      .then((res) => {
        setMovies(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    rows: 2,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  return (
    <Container className="MuiContainer-root pt-5 pb-5 ">
      <h2 className="text-3xl bg-blue-950 py-2 px-4 rounded text-white m-5">
        LỰA CHỌN PHIM
      </h2>
      <Slider {...settings}>
        {movies.map((item) => (
          <div key={item.id}>
            <CardMovie movieCard={item} />
          </div>
        ))}
      </Slider>
    </Container>
  );
}
