import { Button, Form, Input, Select, message } from "antd";
import "./RegisterPage.css";
import React from "react";
import { Option } from "antd/es/mentions";
import { userService } from "../../services/userService";
import { useNavigate } from "react-router-dom";

const prefixSelector = (
  <Form.Item name="prefix" noStyle>
    <Select
      style={{
        width: 70,
      }}
    >
      <Option value="86">+84</Option>
    </Select>
  </Form.Item>
);

export default function RegisterPage() {
  let navigate = useNavigate();
  const onFinish = (values) => {
    const taiKhoan = values.taiKhoan;
    const matKhau = values.matKhau;
    const email = values.email;
    const soDt = values.soDt;
    const hoTen = values.hoTen;
    const maNhom = values.maNhom;
    const formRegister = { taiKhoan, matKhau, email, soDt, hoTen, maNhom };
    console.log("Received values of form: ", values);
    userService
      .postLRegister(formRegister)
      .then((res) => {
        console.log(res);
        message.success("Đăng ký thành công");
        navigate(`/login`);
      })
      .catch((err) => {
        console.log(err);
        message.error("Đăng ký thất bại");
      });
  };
  return (
    <div>
      <div className="register-page">
        <div className="register-container w-1/2">
          <h1 className="font-bold text-2xl pb-5">Đăng ký</h1>
          <Form
            name="normal_register"
            className="register-form"
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
          >
            <Form.Item
              name="taiKhoan"
              label="Tài Khoản"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập tên tài khoản của bạn!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="matKhau"
              label="Mật Khẩu"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập mật khẩu của bạn!",
                },
              ]}
              hasFeedback
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              name="email"
              label="E-mail"
              rules={[
                {
                  type: "email",
                  message: "Email không có giá trị!",
                },
                {
                  required: true,
                  message: "Vui lòng điền Email của bạn!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="soDt"
              label="Số điện thoại"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập số điện thoại của bạn!",
                },
              ]}
            >
              <Input
                addonBefore={prefixSelector}
                style={{
                  width: "100%",
                }}
              />
            </Form.Item>
            <Form.Item
              name="hoTen"
              label="Họ Tên"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập tên của bạn!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="maNhom"
              label="Mã Nhóm"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập mã nhóm của bạn!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="register-form-button"
            >
              Đăng ký
            </Button>
          </Form>
        </div>
      </div>
    </div>
  );
}
